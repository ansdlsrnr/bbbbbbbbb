package com.example.jungsu.calc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.util.Log;

import com.example.jungsu.calc.adapter.RandomListAdapter;
import com.example.jungsu.calc.adapter.RandomListItem;

import java.util.ArrayList;

/**
 * Created by jungsu on 16. 6. 7..
 */
public class ListActivity extends Activity implements View.OnClickListener {
    ArrayList<RandomListItem> mArray;
    RandomListAdapter mAdapter;

    ArrayList<String> mOpArray;
    ListView mListview;

    Button mBtnRefresh;
    Button mBtnResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        mListview = (ListView)findViewById(R.id.listactivity_lv);
        mBtnRefresh = (Button)findViewById(R.id.btn_refresh);
        mBtnResult = (Button)findViewById(R.id.btn_result);

        mArray = new ArrayList<>();
        mOpArray = new ArrayList<>();

        mOpArray.add("+");
        mOpArray.add("-");
        mOpArray.add("x");
        mOpArray.add("/");


        mBtnRefresh.setOnClickListener(this);
        mBtnResult.setOnClickListener(this);

        startRandomMath();
        setItemAdapter();
    }

    private void startRandomMath() {
        mArray.clear();
        for(int i=0; i<20; i++) {
            int no1 = (int) (Math.random()*10) + 1;
            int no2 = (int) (Math.random()*10) + 1;
            int opIdx = (int) (Math.random()*mOpArray.size());

            RandomListItem item = new RandomListItem();
            item.setNo1(no1);
            item.setNo2(no2);
            item.setOp(mOpArray.get(opIdx));

            mArray.add(item);
        }


    }

    private void setItemAdapter() {
        if(mAdapter==null) {
            mAdapter = new RandomListAdapter(this, R.layout.activity_list_item, mArray);
            mListview.setAdapter(mAdapter);
            mListview.setItemsCanFocus(true);
        } else {
            mAdapter.setAdapter(mArray);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_result:
                answer();
                break;
            case R.id.btn_refresh:
                startRandomMath();
                setItemAdapter();
                break;
        }
    }

    private void answer() {
        int cnt=0;
        for(int i=0; i<mArray.size(); i++) {
            double answer = -1111111;
            double result = -52361;
            if(mArray.get(i).getResult().length()>0) {
                answer = chkAnswer(mArray.get(i).getNo1(), mArray.get(i).getNo2(), mArray.get(i).getOp());
                result = Double.parseDouble(mArray.get(i).getResult());
            }
            if(answer == result) {
                cnt++;
            }
        }
        Toast.makeText(this, cnt+"개의 문제를 맞췄습니다.", Toast.LENGTH_SHORT).show();
    }

    private double chkAnswer(int no1, int no2, String op) {
        double ret;
        if(op.equals("+")) {
            ret = no1 + no2;
        } else if(op.equals("-")) {
            ret = no1 - no2;
        } else if(op.equals("x")) {
            ret = no1 * no2;
        } else if(op.equals("/")) {
            ret = (double)((double)no1 / (double)no2);
            String s = String.format("%.02f", ret);
            ret = Double.parseDouble(s);
            Log.d("TAG", "res : " + ret);

        } else {
            ret = -10000000;
        }
        return ret;
    }
}
