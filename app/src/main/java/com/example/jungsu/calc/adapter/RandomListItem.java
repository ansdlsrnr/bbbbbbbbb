package com.example.jungsu.calc.adapter;

/**
 * Created by jungsu on 16. 6. 7..
 */
public class RandomListItem {
    int no1;
    int no2;
    String op;
    String result="";

    public int getNo1() {
        return no1;
    }

    public void setNo1(int no1) {
        this.no1 = no1;
    }

    public int getNo2() {
        return no2;
    }

    public void setNo2(int no2) {
        this.no2 = no2;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
