package com.example.jungsu.calc;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView mBtn0,mBtn1,mBtn2,mBtn3,mBtn4,mBtn5,mBtn6,mBtn7,mBtn8,mBtn9;
    TextView mBtnJum, mBtnResult, mBtnBack, mBtnClear, mBtnAdd,mBtnSub,mBtnMulti,mBtnDivision, mBtnPercent;

    TextView mTxtResult;

    ArrayList<String> operatorList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTxtResult = (TextView)findViewById(R.id.view);

        mBtn0 = (TextView)findViewById(R.id.btn_0);
        mBtn1 = (TextView)findViewById(R.id.btn_1);
        mBtn2 = (TextView)findViewById(R.id.btn_2);
        mBtn3 = (TextView)findViewById(R.id.btn_3);
        mBtn4 = (TextView)findViewById(R.id.btn_4);
        mBtn5 = (TextView)findViewById(R.id.btn_5);
        mBtn6 = (TextView)findViewById(R.id.btn_6);
        mBtn7 = (TextView)findViewById(R.id.btn_7);
        mBtn8 = (TextView)findViewById(R.id.btn_8);
        mBtn9 = (TextView)findViewById(R.id.btn_9);
        mBtnJum = (TextView)findViewById(R.id.btn_jum);
        mBtnResult = (TextView)findViewById(R.id.btn_result);
        mBtnAdd = (TextView)findViewById(R.id.btn_add);
        mBtnSub = (TextView)findViewById(R.id.btn_sub);
        mBtnPercent = (TextView)findViewById(R.id.btn_percnet);
        mBtnBack = (TextView)findViewById(R.id.btn_back);
        mBtnMulti = (TextView)findViewById(R.id.btn_multi);
        mBtnDivision = (TextView)findViewById(R.id.btn_division);
        mBtnClear = (TextView)findViewById(R.id.btn_clear);


        mBtn0.setOnClickListener(this);
        mBtn1.setOnClickListener(this);
        mBtn2.setOnClickListener(this);
        mBtn3.setOnClickListener(this);
        mBtn4.setOnClickListener(this);
        mBtn5.setOnClickListener(this);
        mBtn6.setOnClickListener(this);
        mBtn7.setOnClickListener(this);
        mBtn8.setOnClickListener(this);
        mBtn9.setOnClickListener(this);
        mBtnJum.setOnClickListener(this);
        mBtnResult.setOnClickListener(this);
        mBtnAdd.setOnClickListener(this);
        mBtnSub.setOnClickListener(this);
        mBtnPercent.setOnClickListener(this);
        mBtnBack.setOnClickListener(this);
        mBtnMulti.setOnClickListener(this);
        mBtnDivision.setOnClickListener(this);
        mBtnClear.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_0:
                chkNoAndInsert("0");
                break;
            case R.id.btn_1:
                chkNoAndInsert("1");
                break;
            case R.id.btn_2:
                chkNoAndInsert("2");
                break;
            case R.id.btn_3:
                chkNoAndInsert("3");
                break;
            case R.id.btn_4:
                chkNoAndInsert("4");
                break;
            case R.id.btn_5:
                chkNoAndInsert("5");
                break;
            case R.id.btn_6:
                chkNoAndInsert("6");
                break;
            case R.id.btn_7:
                chkNoAndInsert("7");
                break;
            case R.id.btn_8:
                chkNoAndInsert("8");
                break;
            case R.id.btn_9:
                chkNoAndInsert("9");
                break;
            case R.id.btn_jum:
                chkAndInsertJum();
                break;
            case R.id.btn_result:
                setCalc();
                break;
            case R.id.btn_add:
                setOperator("+");
                break;
            case R.id.btn_sub:
                setOperator("-");
                break;
            case R.id.btn_percnet:
                setOperator("%");
                break;
            case R.id.btn_back:
                lastCharRemove();
                break;
            case R.id.btn_multi:
                setOperator("x");
                break;
            case R.id.btn_division:
                setOperator("÷");
                break;
            case R.id.btn_clear:
                mTxtResult.setText("");
                break;
        }
    }

    private void chkNoAndInsert(String no) {
        if(mTxtResult.getText().toString().startsWith("0") && mTxtResult.getText().toString().startsWith("0.")==false) {
            if(no.equals("0")==false) {
                mTxtResult.setText(no);
            }
        } else {
            mTxtResult.append(no);
        }
    }

    private void chkAndInsertJum() {
        ArrayList<String> numberList = new ArrayList<String>();
        StringTokenizer st = new StringTokenizer(mTxtResult.getText().toString(),"x÷+-");

        while( st.hasMoreTokens() ) {
            String number = st.nextToken();
            numberList.add(number);
        }

        boolean isJum = numberList.get(numberList.size()-1).contains(".");
        if(isJum==false) {
            mTxtResult.append(".");
        }
    }

    private void lastCharRemove() {
        String str= mTxtResult.getText().toString();
        if(str.length()==0) return;
        str = str.substring(0, str.length()-1);
        mTxtResult.setText(str);
    }

    private void setOperator(String op) {
        String str = mTxtResult.getText().toString();
        char strC = str.charAt(mTxtResult.length()-1);

        int cnt=0;
        for(String sp : operatorList) {
            if (strC==sp.charAt(0)) {
                if(op.equals(strC)==false) {
                    lastCharRemove();
                    mTxtResult.append(op);
                }
                cnt++;
                break;
            }
        }
        if(cnt==0) {
            mTxtResult.append(op);
        }
        if(op.equals("%")==false) {
            operatorList.add(op);
        }
    }

    private void setCalc() {
        ArrayList<Double> numberList = new ArrayList<Double>();
        StringTokenizer st = new StringTokenizer(mTxtResult.getText().toString(),"x÷+-");

        boolean doubleValue = mTxtResult.getText().toString().contains(".");

        while( st.hasMoreTokens() ) {
            String no = st.nextToken();
            double number;
            if(String.valueOf(no).contains("%")) {
                number = Double.parseDouble(no.replace("%",""))/100;
                doubleValue = true;
            } else {
                number = Double.parseDouble(no);
            }
            numberList.add( number );
        }

        double result = numberList.get(0);
        for( int i = 0 ; i < operatorList.size() ; i++ ) {
            String operator = operatorList.get(i);

            if(numberList.size()<=i+1) break;

            if( "x".equals(operator)){
                result = ( result * numberList.get(i+1));
            }else if( "÷".equals(operator)){
                doubleValue=true;
                result = ( result / numberList.get(i+1));
            }else if( "+".equals(operator)){
                result = ( result + numberList.get(i+1));
            }else if( "-".equals(operator)){
                result = ( result - numberList.get(i+1));
            }
        }
        operatorList.clear();
        numberList.clear();

        if(doubleValue) {
            mTxtResult.setText(result + "");
        } else {
            mTxtResult.setText((int)result+"");
        }
    }
}
