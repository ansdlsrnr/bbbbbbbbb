package com.example.jungsu.calc.adapter;

import android.content.Context;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jungsu.calc.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by dev on 2016. 6. 14..
 */
public class ImageItemAdapter extends ArrayAdapter<ImageItem> {

//    int[] resId = {R.drawable.bre, R.dr};
    int[] resId = {R.drawable.bre, R.drawable.i12, R.drawable.i13, R.drawable.i14};
    LayoutInflater m_inflater;
    int m_nResource;
    ArrayList<ImageItem> mArray;
    Context cont;
    public ImageItemAdapter(Context context, int resource, ArrayList<ImageItem> obj) {
        super(context, resource, obj);

        cont = context;

        m_nResource = resource;
        this.m_inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mArray = obj;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d("getview start", "");
        convertView = m_inflater.inflate(m_nResource, parent, false);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.img_01);
        final TextView textView = (TextView) convertView.findViewById(R.id.item_txt_no1);
        TextView textView2 = (TextView) convertView.findViewById(R.id.item_txt_no2);

        ImageItem item = getItem(position);

        imageView.setImageResource(resId[item.getResId()]);

        textView.setText("11" + item.getNo());
        textView2.setText("" + position);

        textView.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View v) {
                Toast.makeText(cont, ""+textView.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        Log.d("getview middle", resId[item.getResId()] + "");

        return convertView;
    }

    @Override
    public ImageItem getItem(int position) {
        return super.getItem(position);
    }
}
