package com.example.jungsu.calc.adapter;

/**
 * Created by dev on 2016. 6. 14..
 */
public class ImageItem {
    int resId;
    String no;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }
}
