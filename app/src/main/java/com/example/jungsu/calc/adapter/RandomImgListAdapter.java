package com.example.jungsu.calc.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.example.jungsu.calc.R;

import java.util.ArrayList;

/**
 * Created by jungsu on 16. 6. 7..
 */
public class RandomImgListAdapter extends ArrayAdapter<RandomListItem> {
//TODO : 인터넷통신은 다음.. 응용으로 이미지뷰 로컬에 해서 돌리기..
    private ArrayList<RandomListItem> items;
    private Context mCtx;
    private int m_nResource;
    private LayoutInflater m_inflater;

    public RandomImgListAdapter(Context context, int resource, ArrayList<RandomListItem> objects) {
        super(context, resource, objects);

        mCtx = context;
        items = objects;
        m_nResource = resource;
        this.m_inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setAdapter(ArrayList<RandomListItem> obj) {
        items = obj;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null)
        {
            holder = new ViewHolder();
            convertView = m_inflater.inflate(m_nResource, parent, false);

            holder.txtNo1 	= (TextView) convertView.findViewById(R.id.item_txt_no1) ;
            holder.txtNo2 	= (TextView) convertView.findViewById(R.id.item_txt_no2) ;
            holder.txtOp 	= (TextView) convertView.findViewById(R.id.item_txt_op) ;
            holder.edtResult 	= (EditText) convertView.findViewById(R.id.item_edt_result) ;

            convertView.setTag(holder);
        }
        else
            holder = (ViewHolder) convertView.getTag();

        final RandomListItem item = getItem(position);
        holder.txtNo1.setText(String.valueOf(item.getNo1()));
        holder.txtNo2.setText(""+item.getNo2());
        holder.txtOp.setText(item.getOp());
        Log.d("TAG", "pos : " + position + " edtResult : " + holder.edtResult.getText().toString() + " save data : " + item.getResult());

        if (holder.textWatcher != null)
            holder.edtResult.removeTextChangedListener(holder.textWatcher);

        holder.textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                item.setResult(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        holder.edtResult.addTextChangedListener(holder.textWatcher);
        holder.edtResult.setText(item.getResult());

        return convertView;

    }

    class ViewHolder {
        TextView txtNo1;
        TextView txtNo2;
        TextView txtOp;
        EditText edtResult;
        TextWatcher textWatcher;
    }

    @Override
    public RandomListItem getItem(int position) {
        return super.getItem(position);
    }



}
