package com.example.jungsu.calc;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.jungsu.calc.adapter.ImageItem;
import com.example.jungsu.calc.adapter.ImageItemAdapter;

import java.util.ArrayList;

/**
 * Created by dev on 2016. 6. 14..
 */
public class ImageListActivity extends Activity {
    ImageItemAdapter mAdapter;

    ArrayList<ImageItem> mArray;
    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list);

        mListView = (ListView) findViewById(R.id.listactivity_lv);

        mArray = new ArrayList<>();

        setRandom();
        setItemAdapter();
    }

    private void setRandom() {
        for (int i = 0; i < 20; i++) {

            int no = (int) ((Math.random() * 100) + 1);
            int resId = (int) ((Math.random() * 3) + 1);

            ImageItem item = new ImageItem();
            item.setResId(resId);
            item.setNo("" + no);

Log.d("for", "" + resId + " , " + no);
            mArray.add(item);
        }
    }


    private void setItemAdapter() {
        if (mAdapter == null) {
            mAdapter = new ImageItemAdapter(this, R.layout.activity_imglist_item, mArray);
            mListView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged(); // Data 만 변경 한다.
        }
    }
}
